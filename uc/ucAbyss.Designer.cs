﻿
namespace Change.uc
{
    partial class ucAbyss
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.webAbyss = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // webAbyss
            // 
            this.webAbyss.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webAbyss.Location = new System.Drawing.Point(0, 0);
            this.webAbyss.MinimumSize = new System.Drawing.Size(20, 20);
            this.webAbyss.Name = "webAbyss";
            this.webAbyss.Size = new System.Drawing.Size(1431, 799);
            this.webAbyss.TabIndex = 0;
            // 
            // ucAbyss
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.webAbyss);
            this.Name = "ucAbyss";
            this.Size = new System.Drawing.Size(1431, 799);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.WebBrowser webAbyss;
    }
}
