﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Change.uc
{
    public partial class ucTask : UserControl
    {
        public ucTask()
        {
            InitializeComponent();
            webTask.ScriptErrorsSuppressed = true; //禁用错误脚本提示 
            webTask.IsWebBrowserContextMenuEnabled = false; //禁用右键菜单 
            webTask.WebBrowserShortcutsEnabled = false; //禁用快捷键 
            webTask.AllowWebBrowserDrop = false;//禁止拖拽
            webTask.ScrollBarsEnabled = true;//禁止滚动条
            webTask.NewWindow += new CancelEventHandler(webBrowser1_NewWindow);  //屏蔽弹出新IE窗口
            string url = "http://www.eveonline.vip/col.jsp?id=105";
            webTask.Navigate(url);
        }
        private void webBrowser1_NewWindow(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
            try
            {
                string url = webTask.Document.ActiveElement.GetAttribute("href");

                webTask.Url = new Uri(url);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void webTask_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }
    }
}
